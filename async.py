import logging
import asyncio
import re
import datetime
from time import sleep
import requests
from requests.exceptions import MissingSchema

logging.basicConfig(level=logging.DEBUG)


class Thing(object):
    depth = 0
    rank = {}

    def __init__(self, url, max_depth=4):
        self.url = url
        self.max_depth = max_depth
        self.loop = asyncio.get_event_loop()

    @asyncio.coroutine
    def get_urls(self, url):
        sleep(5)
        try:
            page = requests.get(url)

        except MissingSchema:
            self.rank.setdefault('errors', 0)
            self.rank['errors'] += 1
            return []

        urls = re.findall(r'href=[\'"]?([^\'" >]+)', page.text)
        for url in urls:
            self.rank.setdefault(url, 0)
            self.rank[url] += 1
        return urls

    @asyncio.coroutine
    def get_from_urls(self, urls):
        print(self.depth, self.depth < self.max_depth)
        print(datetime.datetime.now())
        self.depth += 1
        if self.depth < self.max_depth:
            for url in urls:
                urls = yield from self.get_urls(url)
            yield from self.get_from_urls(urls)

    def make_rank(self):
        try:
            self.loop.run_until_complete(self.get_from_urls([self.url]))
        finally:

            self.loop.close()


THING = Thing('http://wp.pl')
THING.make_rank()
print(THING.rank)
print(len(THING.rank.keys()))
